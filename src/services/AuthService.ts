import type IUser from '@/interfaces/IUsers' //Interfaz
import { ref } from 'vue' // función de reactividad
import type { Ref } from 'vue' // La interfaz de reactividad (ref)

//AQUI ES PARA OBTENER LA INFORMACIÓN DEL ENV
const url ='http://172.16.107.120:3000'

//Declarando y exportando la clase
export default class PostUser {
  //Datos reactivos
  private users: Ref<IUser[]>
  private user: Ref<IUser>

  //AQUI SE INICIALIZAN LOS DATOS
  constructor() {
    this.users = ref([])
    this.user = ref({}) as Ref<IUser>
  }
  //Son Los getters
  getPosts(): Ref<IUser[]> {
    return this.users
  }
  getPost(): Ref<IUser> {
    return this.user
  }

  async fetchAll(): Promise<void> {
    try {
      const json = await fetch(url + '/Users')
      const response = await json.json()
      this.users.value = await response
    } catch (error) {
      //Agrega aqui lo que necesites
      console.log(error)
    }
  }

  async fetchUser(email: string): Promise<void>{
    try{
        const json = await fetch(url + '/Users')
        const response = await json.json()
        this.user.value = await response
    } catch(error){
        console.log(error)
    }
  }

  async fetchPost(email:string, name:string, password:string): Promise<void> {
    try {
      const json = await fetch(url + '/Register/?' + email)
      const response = await json.json()
      this.user.value = await response
    } catch (error) {
      console.log(error)
    }
  }
}
