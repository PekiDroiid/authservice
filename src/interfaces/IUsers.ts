export default interface IPost {
    name: string
    email: string
    password: string
}