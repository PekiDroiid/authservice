import { createRouter, createWebHistory } from 'vue-router'

import PostUser from '@/components/PostUser.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'post',
      component: PostUser
    }
  ]
})

export default router
